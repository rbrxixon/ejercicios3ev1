package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.regex.Pattern;

public class AdivinaNumero2 {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    String entrada = null;
    int numeroGenerado, numeroMax, numeroMin, numeroSuperior = 0;
    Random r = null;
    boolean seguir = true;
    boolean error = false;
    Pattern patron_entero = Pattern.compile("\\d+");
    int opcion = 0;
    
	public void escribirNumero() {
	   try {
		   do {
			System.out.println("Escribe cual es el limite del valor superior del numero que has pensado (debe ser mayor de 1000) ");
			entrada = reader.readLine();
			comprobarSiEsNumero(entrada, patron_entero);
		   } while (error);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    	
    public void menuElegido (int opcion) {
    	switch (opcion) {
		case 1:
			System.out.println("Mayor");
			numeroMin = numeroGenerado+1; 
			buscarAleatorio(numeroMin,numeroMax);
			break;
		case 2:
			System.out.println("Menor");
			numeroMax = numeroGenerado-1; 
			buscarAleatorio(numeroMin,numeroMax);
			break;
		case 3:
			System.out.println("Acierto");
			seguir = false;
			break;
		default:
			System.out.println("Error, debe seleccionar una opcion disponible (1-3)");
			break;
		}
    }
    
    public boolean comprobarSiEsNumero(String s, Pattern patron) {
		if (patron.matcher(entrada).matches()) {
			error = false;
			return true;
		} else {
			System.out.println("Error, debe escribir un numero");
			error = true;
			return false;
		}
    }
    
    public int leerOpcion() {
		try {	
			do {
				entrada = reader.readLine();
				comprobarSiEsNumero(entrada, patron_entero);
				if (error)
					mostrarMenu();
			} while (error);
				opcion = Integer.parseInt(entrada);		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return opcion;
    }
    
    public void mostrarMenu() {
		System.out.println("1. Mayor\n2. Menor\n3. Acierto");
    }
	
	public int buscarAleatorio() {
		numeroSuperior = Integer.parseInt(entrada);
		numeroMax = numeroSuperior;
		numeroMin = 1000;
		r = new Random();
		numeroGenerado = r.nextInt(numeroSuperior-numeroMin)+numeroMin;
		System.out.println("El numero que has pensado es " + numeroGenerado + " ?");
		return numeroGenerado;
	}
	
	public int buscarAleatorio(int inferior, int superior) {
		if (inferior == superior) {
			System.out.println("Estoy seguro de que me has mentido ;)");
			System.exit(0);
		}
		
		System.out.println("Inferior: " + inferior + " | Superior: " + superior);
		r = new Random();
		numeroGenerado = r.nextInt(superior-inferior)+inferior;
		System.out.println("El numero que has pensado es " + numeroGenerado + " ?"); 
		return numeroGenerado;
	}
	
	public static void main(String[] args) {
		AdivinaNumero2 a = new AdivinaNumero2();
		a.escribirNumero();
		a.buscarAleatorio();
		do {
			a.mostrarMenu();
			a.menuElegido(a.leerOpcion());
			
		} while (a.seguir);
		
		
		

	}

}
