package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TrianguloFloyd {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	    String entrada = null;
	    int opcion, contador = 1;
	    
	    try {
	    	System.out.print("Cuantas filas quieres generar?:");
			entrada = reader.readLine();
			opcion = Integer.parseInt(entrada);	
			for (int x = 0; x <= opcion; x++) {
				for (int y = 0; y < x; y++) {
					System.out.print(contador++ + " ");
				}
				System.out.println();
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   

	}

}
