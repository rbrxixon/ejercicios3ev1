package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Triángulo {

	public static void main(String[] args) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	        String entrada = null;
	        float a,b,c = 0;
			try {
				System.out.print("Introduce lado A: ");
				entrada = reader.readLine();
				a = Float.parseFloat(entrada);
				System.out.print("Introduce lado B: ");
				entrada = reader.readLine();
				b = Float.parseFloat(entrada);
				
				System.out.print("Introduce lado C: ");
				entrada = reader.readLine();
				c = Float.parseFloat(entrada);
				
				System.out.println("A: " + a + " B: " + b + " C: " + c);
				
				if (a==b && b==c) {
		               System.out.println("El Triangulo es Equilatero");
				} else if (a==b || a==c || b==c) {
					System.out.println("El Triangulo es Isoceles");
				} else if (a!=b || a!=c || b!=c) {
					System.out.println("El Triangulo es Escaleno");
	            }		

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
