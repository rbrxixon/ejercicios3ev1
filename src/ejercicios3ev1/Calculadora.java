package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class Calculadora {
    Pattern patron_entero = Pattern.compile("\\d+");
    Pattern patron_decimales = Pattern.compile("[-]?[0-9]*\\.?[0-9]+"); 
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    String entrada = null;
    int numero, opcion = 0;
    boolean error = false;
    float n1,n2 = 0;
    
    public boolean comprobarSiEsNumero(String s, Pattern patron) {
		if (patron.matcher(entrada).matches()) {
			error = false;
			return true;
		} else {
			System.out.println("Error, debe escribir un numero");
			
			error = true;
			return false;
		}
    }
    
    public void mostrarMenu() {
		System.out.println("1. Sumar\n2. Restar\n3. Multiplicar\n4. Dividir\n0. Salir");
		System.out.print("Escriba la opción seleccionada: ");
    }
    
    public void escribirNumeros () {
    	
		try {
			System.out.println("Si escribe decimales, es necesario con . (ejemplo: 0.29)");
			System.out.println("Escribe el numero 1");
			entrada = reader.readLine();
			comprobarSiEsNumero(entrada, patron_decimales);
			n1 = Float.parseFloat(entrada);		
			
			System.out.println("Escribe el numero 2");
			entrada = reader.readLine();
			comprobarSiEsNumero(entrada, patron_decimales);
			n2 = Float.parseFloat(entrada);		
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public boolean menuElegido (int opcion) {
    	switch (opcion) {
		case 1:
			System.out.println("Sumar");
			break;
		case 2:
			System.out.println("Restar");
			break;
		case 3:
			System.out.println("Multiplicar");
			break;
		case 4:
			System.out.println("Dividir");
			break;
		case 0:
			System.out.println("Salir");
			System.exit(0);
			break;
		default:
			System.out.println("Error, debe seleccionar una opcion disponible (0-4)");
			mostrarMenu();
			leerOpcion();
			break;
		}
		return error;
    }
    
    public float ejecutarOperacion () {
    	float resultado = 0;
    	switch (opcion) {
		case 1:
			resultado = n1+n2;
			System.out.println("El resultado de la suma es: ");
			break;
		case 2:
			resultado = n1-n2;
			System.out.println("El resultado de la resta es: ");
			break;
		case 3:
			resultado = n1*n2;
			System.out.println("El resultado de la multiplicacion: ");
			break;
		case 4:
			resultado = n1/n2;
			System.out.println("El resultado de la dividision: ");
			break;
		case 0:
			System.out.println("Salir");
			System.exit(0);
			break;
		default:
			System.out.println("Error, debe seleccionar una opcion disponible (0-4)");
			mostrarMenu();
			leerOpcion();
			break;
		}
    	return resultado;
    }
    
    public int leerOpcion() {
		try {	
			do {
				entrada = reader.readLine();
				comprobarSiEsNumero(entrada, patron_entero);
				if (error)
					mostrarMenu();
			} while (error);
				opcion = Integer.parseInt(entrada);		
				menuElegido(opcion);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return opcion;
    }
    
	public static void main(String[] args) {
		System.out.println("Calculadora");
		Calculadora c = new Calculadora(); 
		c.mostrarMenu();
		c.leerOpcion();
		c.escribirNumeros();
		System.out.println(c.ejecutarOperacion());
	}

}
