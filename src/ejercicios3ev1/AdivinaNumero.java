package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class AdivinaNumero {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    String entrada = null;
    int numeroAleatorio,n, numero = 0;
    Random r = null;
    boolean seguir = true;
    
	public void escribirNumero() {
	   try {
			System.out.println("He pensado un número entre 1 y N, adivina cuál es: ");
			entrada = reader.readLine();
			numero = Integer.parseInt(entrada);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void generarAleatorio() {
		r = new Random();
		n = r.nextInt(100000-1000)+1000;
		numeroAleatorio = r.nextInt(n-1)+1;
//		System.out.println("numeroAleatorio: " + numeroAleatorio );
	}
	
	public void comprobarNumero() {
		if (numeroAleatorio > numero) {
			System.out.println("El numero que he pensado es mayor");
		} else if (numeroAleatorio < numero ) {
			System.out.println("Es menor");
		} else if (numero == numeroAleatorio) {
			System.out.println("Has acertado!!!");
			seguir = false;
		}
	}

	public static void main(String[] args) {

		AdivinaNumero a = new AdivinaNumero();
		a.generarAleatorio();
		do {
			a.escribirNumero();
			a.comprobarNumero();
		} while (a.seguir);
			
		
				
			
			
	

	}

}
