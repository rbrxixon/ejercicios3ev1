package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class Hora {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        String entrada = null;
        Date fecha = null;
        String tiempo = null;
        int horas, minutos, segundos = 0;
        
		try {
			fecha = new Date();
			System.out.print("Introduce hora (hh): ");
			entrada = reader.readLine();
			horas = Integer.parseInt(entrada);
			fecha.setDate(horas);
			
			System.out.print("Introduce minutos (mm): ");
			entrada = reader.readLine();
			minutos = Integer.parseInt(entrada);
			fecha.setMinutes(minutos);
			
			System.out.print("Introduce segundos (mm): ");
			entrada = reader.readLine();
			segundos = Integer.parseInt(entrada);
			segundos++;
			fecha.setSeconds(segundos);
			
			tiempo = horas + ":" + minutos + ":" + segundos; 
			
			System.out.println("hora: " + tiempo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
