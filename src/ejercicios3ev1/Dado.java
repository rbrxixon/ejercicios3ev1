package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Dado {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	    String entrada = null;
	    int opcion, tirada = 0;
	    Random r = null;
	    try {
	    	r = new Random();
	    	System.out.print("Cuantas veces quieres tirar el dado?: ");
			entrada = reader.readLine();
			opcion = Integer.parseInt(entrada);	
			
			for (int i = 0; i < opcion; i++) {
				tirada = r.nextInt(6-1)+1;
				System.out.println("Ha salido el " + tirada);
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
