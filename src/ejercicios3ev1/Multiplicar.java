package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class Multiplicar {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
    String entrada = null;
    int numero;
    Pattern patron = Pattern.compile("\\d+");
    int aciertos, fallos = 0;
    boolean seguir = true;
    
    public void reset (String mensaje) {
    	System.out.println("RESET: " + mensaje);
    	tablaARepasar();
    }
    
    public boolean comprobarSiEsNumero(String s) {
		if (patron.matcher(entrada).matches())
			return true;
		return false;
    }
    
    public boolean comprobarSiEntre1y9(int n) {
		if (n >= 1 && n <= 9 )
			return true;
		return false;
    }
    
    
	public int tablaARepasar () {
		System.out.print("Introduce tabla a repasar (numero entre 1 y 9): ");
		try {
			entrada = reader.readLine();
			if ( ! comprobarSiEsNumero(entrada))
				reset("No es un numero");
			
			numero = Integer.parseInt(entrada);
			if ( ! comprobarSiEntre1y9(numero))
				reset("Debe ser un numero entre 1 y 9");
						
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return numero;
	}
	
	public void comprobarResultado (int entrada, int resultado){
		if (resultado == entrada)
			aciertos++;
		else {
			fallos++;
			System.out.println("Has fallado. El resultado correcto es: " + resultado);
		}
	}
	
	public int escribirNumero (int tabla, int multiplicador) {
		try {
			System.out.print(tabla + "x" + multiplicador + "= ");
			entrada = reader.readLine();
			
			if (! comprobarSiEsNumero(entrada)) {
				System.out.println("Escribe un numero correcto");
				comprobarResultado(tabla,multiplicador);
			}
			numero = Integer.parseInt(entrada);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return numero;
	}
	
	public void comprobarPuntuacion() {
		if (fallos < 2)
			System.out.println("Has aprobado");
		else
			System.out.println("Has suspendido");
	}
	
	public void preguntarSiContinuar() {
		System.out.println("¿Deseas repasar otra tabla o salir del programa? [S/N]");
		try {
			entrada = reader.readLine();
			if (! entrada.equals("S")) { 
				System.out.println("Salir del programa");
				seguir = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void repasarTabla(int tabla) {
		System.out.println("Repasar la tabla del numero " + tabla + "\nEscribe los resultados de las multiplicaciones");
		int resultadoCorrecto = 0;
        for (int i = 1; i <= 10; i++) {
        	resultadoCorrecto=tabla*i;
        	comprobarResultado(escribirNumero(tabla, i),resultadoCorrecto);
		}
        comprobarPuntuacion();
        preguntarSiContinuar();   
	}

	public static void main(String[] args) {
		Multiplicar m = new Multiplicar();
		while (m.seguir) {
			
	        m.repasarTabla(m.tablaARepasar());
		} 

	}

}
