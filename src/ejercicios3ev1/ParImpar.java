package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ParImpar {

	public static void main(String[] args) {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        String entrada = null;
        int numero;
        System.out.print("Introduce numero: ");
		try {
			entrada = reader.readLine();
			numero = Integer.parseInt(entrada);
			if ( numero % 2 == 0 ) {
			        System.out.printf( "ES PAR" );
			} else {
			        System.out.printf( "ES IMPAR" );
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

}
