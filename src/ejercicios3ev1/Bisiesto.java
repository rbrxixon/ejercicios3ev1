package ejercicios3ev1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bisiesto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        String entrada = null;
        int anio;
        System.out.print("Introduce año: ");
		try {
			entrada = reader.readLine();
			anio = Integer.parseInt(entrada);
			
			if ((anio % 4 == 0) && (anio % 100 != 0) && (anio % 400 != 0)) {
				System.out.println("BISIESTO");
			} else {
				System.out.println("NO BISIESTO");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
